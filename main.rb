require 'active_support/all'
require 'openssl'
require 'yaml'
require 'addressable'
require 'active_model'
require 'gitlab_chronic_duration'

require "zeitwerk"

####### mocks needed for not editing the files

# namespace for zeitwerk
module Gitlab
  module Metrics
    module System
      def self.monotonic_time
        Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second)
      end
    end
  end
end

# ee methods
module InjectEnterpriseEditionModule
  def prepend_if_ee(constant, with_descendants: false)
    return
  end

  def extend_if_ee(constant)
    return
  end

  def include_if_ee(constant)
    return
  end
end

Module.prepend(InjectEnterpriseEditionModule)

# idk
def require_dependency(arg)
  return
end

# use default features
module Feature
  def self.enabled?(key, thing = nil, type: :development, default_enabled: false)
    return default_enabled
  end
end

# configure autoloader
loader = Zeitwerk::Loader.new
loader.push_dir("#{__dir__}/gitlab/lib/gitlab",  namespace: Gitlab)
loader.push_dir("#{__dir__}/gitlab/lib/")
loader.setup # ready!

# fake Repository
class Repository
  def root_ref_sha
    return "HEAD"
  end

  def blob_data_at(ctx, location)
    if location.start_with? "."
      return File.read(location)
    else
      return File.read("./"+ location)
    end
  end
end

class PredefinedVariables
  def to_runner_variables
    return {}    
  end
end

# fake Project
class Project
  attr_accessor :repository
  attr_accessor :predefined_variables
  
  def initialize()
    @repository = Repository.new()
    @predefined_variables = PredefinedVariables.new()
  end

end

####### end of mocks


def generate_yaml
  inputPath = ""
  if ARGV.length < 1
    inputPath = './.gitlab-ci.yml'
  else 
    inputPath = ARGV[0]
  end

  outputPath = ""
  if ARGV.length < 2
    outputPath = './.gitlab-ci-all.yml'
  else 
    outputPath = ARGV[1]
  end

  unless File.exist?(inputPath)
    puts "File does not exist"
    exit(1)
  end

  config = File.read(inputPath)
  config_processor = Gitlab::Ci::YamlProcessor.new(config, { project: Project.new() }).execute

  unless config_processor.valid?
    puts "Failed to parse:"
    puts config_processor.errors
    exit(1)
  end

  # removes colons from keys
  stringifiedConfig = config_processor.instance_variable_get("@ci_config").instance_variable_get("@config").deep_stringify_keys!
  # setting line_width to a high value, It is to prevent line wrapping as it may break the scripts
  File.write(outputPath, stringifiedConfig.to_yaml({:line_width => 10000}))
end


generate_yaml
