#!/bin/bash

set -e
set -u

./dl_dep.sh
docker build -t gitlab-ci-processor .
